//
//  ViewController.swift
//  coffee_ViniciusRezende
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Kingfisher
import Alamofire

class ViewController: UIViewController {
    struct coffe: Decodable {
        let file:String
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var imgCoffe: UIImageView!
    
    @IBAction func btnOutraFoto(_ sender: Any) {
          getCoffe()
        }
    func getCoffe(){
        
        AF.request("https://coffee.alexflipnote.dev/random.json")
            .responseDecodable(of: coffe.self){
                response in

                if let coffe = response.value{
                    self.imgCoffe.kf.setImage(with: URL(string: coffe.file)) { _ in
                        
                    }
                    
                }
            }
    }
}


